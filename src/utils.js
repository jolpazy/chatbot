export const getTimeStamp = () => {
  const d = new Date();
  let h = d.getHours();
  let m = d.getMinutes();
  if (m < 10) {
    m = `0${m}`;
  }
  if (h < 10) {
    h = `0${h}`;
  }
  return `${h}:${m}`;
};

export const wait = ms => new Promise(resolve => setTimeout(resolve, ms));
