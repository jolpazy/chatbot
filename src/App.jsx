import React, { Component } from "react";
import RiveScript from "rivescript";
import brain from "./brain.rive";
import styled from "styled-components";
import { wait } from "./utils";

import UserMessage from "./UserMessage";
import BotMessage from "./BotMessage";
import Typing from "./Typing";

const AppWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  background-color: ${props => props.theme.lightGray};
`;

const ChatBox = styled.div`
  border-radius: 4px;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  width: 400px;
  height: 600px;
  background-color: ${props => props.theme.chatBoxBackground};
  @media (max-height: 700px) {
    height: 90%;
  }
  @media (max-width: 400px) {
    width: 95%;
  }
`;

const MessagesWrap = styled.div`
  flex-direction: column;
  display: flex;
  overflow: auto;
  width: 100%;
  &::-webkit-scrollbar {
    width: 4px;
  }
  &::-webkit-scrollbar-thumb {
    border-radius: 4px;
    background-color: ${props => props.theme.textBlue};
  }
`;

const InputWrap = styled.div`
  width: 100%;
  display: flex;
  min-height: 48px;
`;

const UserInput = styled.input`
  border: transparent;
  border-radius: 4px;
  box-shadow: none;
  width: 100%;
  min-height: 30px;
  padding: 4px;
  margin: 4px;
  background-color: ${props => props.theme.lightGray};
  :focus {
    outline: none;
  }
`;

class App extends Component {
  bot = new RiveScript();

  state = {
    messages: [],
    typing: false
  };

  messageBoxRef = React.createRef();

  componentDidMount() {
    this.bot
      .loadFile(brain)
      .then(this.onReady)
      .catch(this.onError);
  }

  handleKeDown = e => {
    const { value } = e.target;
    if (!value) {
      return;
    }

    if (e.key === "Enter") {
      this.handleUserInput(value);

      e.target.value = "";

      this.bot.reply("local-user", value).then(reply => {
        //Guaranteed 300ms wait before random wait time in range of 1.5 seconds
        const timeToReply = Math.floor(Math.random() * 1500) + 301;
        const timeToType = reply.length * 40;

        wait(timeToReply)
          .then(() => {
            this.setState({ typing: true }, () => this.setScroll());
          })
          .then(() => {
            wait(timeToType).then(() => {
              this.handleReply(reply);
            });
          });
      });
    }
  };

  setScroll = () => {
    const element = this.messageBoxRef.current;
    element.scrollTop = element.scrollHeight;
  };

  handleUserInput = value => {
    this.setState(
      {
        messages: this.state.messages.concat({
          content: value,
          owner: "user"
        })
      },
      () => this.setScroll()
    );
  };

  handleReply = value => {
    this.setState(
      {
        messages: this.state.messages.concat({
          content: value,
          owner: "bot"
        }),
        typing: false
      },
      () => this.setScroll()
    );
  };

  onReady = () => {
    this.bot.sortReplies();
  };

  onError = () => console.error("Error");

  render() {
    return (
      <React.Fragment>
        <AppWrapper>
          <ChatBox>
            <MessagesWrap innerRef={this.messageBoxRef}>
              {this.state.messages.map((m, i) => {
                if (m.owner === "bot") {
                  return <BotMessage key={i} content={m.content} />;
                }
                if (m.owner === "user") {
                  return <UserMessage key={i} content={m.content} />;
                }
                return null;
              })}
              <Typing condition={this.state.typing} />
            </MessagesWrap>
            <InputWrap>
              <UserInput
                onKeyDown={this.handleKeDown}
                placeholder="Type a message to the bot.."
              />
            </InputWrap>
          </ChatBox>
        </AppWrapper>
      </React.Fragment>
    );
  }
}

export default App;
