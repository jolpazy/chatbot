import React from 'react'
import styled from 'styled-components'
import { Message } from './Message'
import { Time } from './Time'

const StyledBotMessage = styled(Message)`
  background-color: ${props => props.theme.botMessageBackground};
  align-self: flex-start;
`

const BotMessage = props => (
  <StyledBotMessage>
    <Time /> {props.content}
  </StyledBotMessage>
)

export default BotMessage
