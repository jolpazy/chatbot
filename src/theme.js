const appTheme = {
  //  Primary colors
  lightGray: '#f9f4f4',
  chatBoxBackground: '#8ab2f2',
  textBlue: '#3071ff',
  scrollBarBlue: '#141e38',
  botMessageBackground: '#fbd6fc',
  userMessageBackground: '#efedc9',
  timeText: '#6d6c6c'
}

export default appTheme
