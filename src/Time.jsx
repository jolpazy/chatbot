import React from 'react'
import styled from 'styled-components'

import { getTimeStamp } from './utils'

const StyledTime = styled.span`
  font-size: 10px;
  color: ${props => props.theme.timeText};
`

export const Time = () => <StyledTime>{getTimeStamp()}</StyledTime>
