import React from "react";
import styled from "styled-components";

const TypingDiv = styled.div`
  align-self: center;
  font-style: italic;
  margin: 8px;
  padding: 8px;
  color: ${props => props.theme.lightGray};
`;

class Typing extends React.Component {
  state = {
    dots: ""
  };

  interval;

  addDots = () => {
    if (this.state.dots.length < 3) {
      this.setState({ dots: `${this.state.dots}.` });
      return;
    }
    this.setState({ dots: "" });
  };

  componentDidMount() {
    this.interval = setInterval(this.addDots, 300);
  }
  componentWillMount() {
    clearInterval(this.interval);
  }
  render() {
    const { condition } = this.props;
    return condition ? (
      <TypingDiv>
        Typing
        {this.state.dots}
      </TypingDiv>
    ) : null;
  }
}

export default Typing;
