import React from 'react'
import styled from 'styled-components'
import { Message } from './Message'
import { Time } from './Time'

const StyledUserMessage = styled(Message)`
  background-color: ${props => props.theme.userMessageBackground};
  align-self: flex-end;
`

const UserMessage = props => (
  <StyledUserMessage>
    {props.content} <Time />
  </StyledUserMessage>
)

export default UserMessage
