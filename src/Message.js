import styled from "styled-components";
export const Message = styled.div`
  border-radius: 4px;
  margin: 4px;
  padding: 8px;
  max-width: 90%;
  word-break: break-word;
  color: ${props => props.theme.darkestBlue};
  div {
    margin: 0 4px;
  }
`;
